from django import template

register = template.Library()


@register.filter(name="resize_to")
def resize_to(value, arg):
    if arg is None:
        return value
    else:
        return value * int(arg)
    # the value is amount
    # the arg is the number of servings but it is returned as a string
