from audioop import reverse
from http.client import HTTPResponse
from django.http import HttpResponseRedirect
from sqlite3 import IntegrityError
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods

from recipes.forms import RatingForm
from recipes.models import Recipe, ShoppingItem, Ingredient


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        try:
            form.is_valid()
            rating = form.save(commit=False)
            rating.recipe = Recipe.objects.get(pk=recipe_id)
            rating.save()
        except Recipe.DoesNotExist:
            return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 5

    # def get_queryset(self):
    #     queryset = super().get_queryset()
    #     print(queryset)
    #     return queryset

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     from pprint import pprint

    #     pprint(context)
    #     return context


class RecipeDetailView(LoginRequiredMixin, DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    # def get_queryset(self):
    #     try:
    #         personal_recipe = Recipe.objects.filter(author=self.request.user)
    #     except TypeError:
    #         return redirect("login")
    #     return personal_recipe

    def get_context_data(self, **kwargs):
        # try:
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        food_items = []
        shopping_items = ShoppingItem.objects.filter(user=self.request.user)
        for item in shopping_items:
            food_items.append(item.food_item)
        context["servings"] = self.request.GET.get("servings")
        context["food_items"] = food_items

        return context

        # context = {"form": form}
        # except TypeError:
        #     return None

        # def render_to_response(self, context, **response_kwargs):
        #     if context is None:
        #         return HttpResponseRedirect(reverse("login"))
        #     return super(RecipeDetailView, self).render_to_response(
        #         context, **response_kwargs
        #     )


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_items/list.html"

    def get_queryset(self):
        products = ShoppingItem.objects.filter(user=self.request.user)
        return products


@require_http_methods(["POST"])
@login_required(login_url="/accounts/login/")
def create_shopping_item(request):
    ingredient_id = request.POST.get("ingredient_id")
    ingredient = Ingredient.objects.get(id=ingredient_id)
    user = request.user
    try:
        ShoppingItem.objects.create(food_item=ingredient.food, user=user)
    except IntegrityError:
        pass
    return redirect("recipe_detail", pk=ingredient.recipe.id)


@require_http_methods(["POST"])
@login_required(login_url="/accounts/login/")
def delete_shopping_item(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_items_list")
