from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth.models import User


def signup(request):
    # POST when they submit the info/data
    if (
        request.method
        == "POST"  # this creates a dictionary of all this info that was submitted
    ):  # GET load the form POST is pushing the submit button
        form = UserCreationForm(
            request.POST
        )  # uses the info in the dictionary in the user creation form
        if form.is_valid():
            # create the user (can do user = form.save() and it will return
            # the thing that was saved (everything in the post) which is the user and we will get all
            #  this info that we just manually created from the save.form() function )
            username = request.POST.get(
                "username"
            )  # taking this from the dictionary that was created
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username, password=password
            )
            # the we will log in the user
            login(request, user)
            # redirect them somwhere (recipe list)
            return redirect("recipes_list")
    # GET (when the page is loaded it creates the empty form and renders it)
    else:
        form = UserCreationForm
    context = {"form": form}
    return render(request, "registration/signup.html", context)


# Create your views here.
