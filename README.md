# Scrumptious Recipes
Recipe website that allows for account creation with login and logout functionality. The user will be able to save recipes and create, edit, and delete recipes and mealplans.

## Design
* Recipes
    * Recipe, Measure, FoodItem, Ingredient, Step, Rating, and ShoppingItem models. Views allow for CRUD functionality. 
    
* Meal_Plans:
    * MealPlan model. Views allow for CRUD functionality. 

* Accounts:
    * Utilizes Django built in UserCreationForm in order to create accounts. 
